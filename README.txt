Scald Gallery Grid is a "player" for the Scald: Gallery module
(https://www.drupal.org/project/scald_gallery), which is an addon module to
the Scald: Media Management module (https://www.drupal.org/project/scald).

This module lets you render gallery items in a grid format. You can choose
how to render column widths, either via CSS Percentages or Bootstrap classes
(https://getbootstrap.com/). You can choose to link grid images to
their full versions, and it integrates with the Colorbox module
(https://www.drupal.org/project/colorbox) and the Lightbox2 module
(https://www.drupal.org//project/lightbox2).
