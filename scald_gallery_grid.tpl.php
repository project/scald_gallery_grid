<?php
/**
 * @file
 * Theme implementation of the Gallery Grid player.
 *
 * Variables:
 * - $options: An array of settings from the player.
 * - $scald_grid_player_id: The ID of the gallery instance.
 * - $items: An array of items to be displayed in the gallery.
 */
?>
<div class="<?php print $classes; ?>">
  <?php foreach ($items as $item): ?>
      <?php print $item; ?>
  <?php endforeach; ?>
</div>
